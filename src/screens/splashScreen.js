import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';

class welcomeScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('loginScreen');
    }, 3000);
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <Image
            style={{width: 144, height: 144}}
            source={require('../assets/MHD.png')}
          />
          <Text
            style={{
              marginTop: 1,
              fontWeight: 'bold',
              fontSize: 37,
              color: '#26398C',
            }}>
            SALT ACADEMY
          </Text>
          <Text
            style={{
              marginTop: 4,
              fontWeight: 'normal',
              fontSize: 12,
              color: '#26398C',
            }}>
            REACT-NATIVE MOBILE HYBRID DEVELOPMENT
          </Text>
        </View>
      </View>
    );
  }
}

export default welcomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FDE74C',
  },
});
