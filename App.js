import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';

import Router from './src/router/index';

class App extends Component {
  render() {
    return <Router />;
  }
}

export default App;

const styles = StyleSheet.create({});
